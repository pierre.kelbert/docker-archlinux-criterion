FROM archlinux/base
MAINTAINER Marc 'risson' Schmitt <marc.schmitt@risson.space>

RUN pacman --noconfirm --noprogressbar -Syyu

RUN pacman --noconfirm --noprogressbar --needed -S make gcc automake \
            clang cmake ctags gcc-libs glibc \
            boost gdb \
            valgrind python python-yaml python-termcolor \
            gtest

COPY ./install_criterion.sh /usr/bin/install_criterion
RUN install_criterion docker
